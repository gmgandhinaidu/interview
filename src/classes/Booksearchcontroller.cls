public with sharing class Booksearchcontroller { 
public list <account> acc {get;set;} 
public string searchstring {get;set;} 
public GoogleBooksResponse result {get;set;}
public List <GoogleBooks> booksList{get;set;}
public List<WrapperClass> listWrapper {get;set;}
    public Booksearchcontroller( ) { 
        //result  = new GoogleBooksResponse ();
        acc = new List<Account>();
        booksList = new List<GoogleBooks>();
        listWrapper  = new List<WrapperClass>();
    }
    public void booksSelectedForReading(){
    List<GoogleBooks> selectedBooksList =  new List<GoogleBooks>();  
    //Set<String> BooksToInsert = new Set<String>();
    List<Books__c> BooksToUpsert = new List<Books__c>();
    List<Reading_List__c> readingListToUpsert = new List<Reading_List__c>();
        for(WrapperClass wrapobj : listWrapper){
            if(wrapobj.selectBook == true){
               //selectedBooksList.add(wrapobj.gbInstance);
               //selectedlistWrapper.add(wrapobj);
               //CasewithCaseComment.put(wrapobj.cs.Id,wrapobj.commenttext); 
               //selectedBooksList.add(wrapobj.gbInstance.volumeInfo.industryIdentifiers[0].identifier);    
               selectedBooksList.add(wrapobj.gbInstance);    
               //system.debug('mg BooksToInsert'+BooksToInsert);         
            }
        }
        for(GoogleBooks isbn : selectedBooksList){
            Books__c bk = new Books__c();
            bk.ISBN_Identifier__c = isbn.volumeInfo.industryIdentifiers[0].identifier;
            bk.Name = isbn.volumeInfo.title;
            BooksToUpsert.add(bk);
        }
        If(BooksToUpsert.size() > 0){
            Upsert BooksToUpsert ISBN_Identifier__c  ;
        }
        
        Id loggedinUserId = UserInfo.getUserId();
        
        for(Books__c bb : BooksToUpsert ){
            Reading_List__c rl = New Reading_List__c();
            rl.Name = bb.Name;
            rl.Books__c = bb.Id;
            rl.User_and_ISBN_combo__c = loggedinUserId+bb.ISBN_Identifier__c ;
            readingListToUpsert.add(rl);
        }
        If(readingListToUpsert.size() > 0){
            Upsert readingListToUpsert User_and_ISBN_combo__c ;
        }
    }
    public void search(){ 
    /*string searchquery='select name,id from account where name like \'%'+searchstring+'%\' Limit 20'; 
    acc= Database.query(searchquery); */
    
        String s;
        String sText= searchstring.replaceAll( '\\s+', '');
        string endpointURL = 'https://www.googleapis.com/books/v1/volumes?q=' + sText;
        HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setHeader('Content-Type','application/json');
                req.setHeader('Accept','application/json');
                //req.setEndpoint('https://www.googleapis.com/books/v1/volumes?q=harry+potter');
                req.setEndpoint(endpointURL);
                Http http = new Http();
                HTTPResponse res = null;
                res = http.send(req);
                s = res.getBody();
                //system.debug('hi'+s);
        result = (GoogleBooksResponse)JSON.deserialize(s , GoogleBooksResponse.class);
        //system.debug('Mohan'+ result);
        booksList = result.items;
        if(booksList.size() > 0) {
            listWrapper = new List<WrapperClass>();
            for(GoogleBooks gb : booksList) {
                listWrapper.add(new WrapperClass(gb, false));
            }
        }   
    } 
    public void clear(){ 
        If(listWrapper <> null)
        listWrapper.clear(); 
    } 
    public class WrapperClass {
            
        public boolean selectBook {get;set;}
        public GoogleBooks gbInstance {get;set;}
        
        public WrapperClass(GoogleBooks gb,boolean selBook){
        
            This.gbInstance = gb;
            This.selectBook = selBook;
            
        }               
    }
}