global class DeleteBooksWithNoChilds implements Database.Batchable<sObject>{

        global DeleteBooksWithNoChilds(){
                   // Batch Constructor
        }
       
        // Start Method
        global Database.QueryLocator start(Database.BatchableContext BC){
            //Date s = system.today()-10;
            string query = 'SELECT Id, Name FROM Books__c WHERE Id NOT IN (SELECT Books__c FROM Reading_List__c)';
            return Database.getQueryLocator(query);
        }
      
      // Execute Logic
       global void execute(Database.BatchableContext BC, List<Books__c>scope){
              if(scope.size() >0)
              system.debug('scope'+scope);
              delete scope;
     
       }
     
       global void finish(Database.BatchableContext BC){
            // Logic to be Executed at finish
       }
    }