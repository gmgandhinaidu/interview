global class ScheduleClassForDeletingBooks implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        DeleteBooksWithNoChilds b = new DeleteBooksWithNoChilds(); //ur batch class
        database.executebatch(b);
    }
}