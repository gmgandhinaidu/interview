public class GoogleBooks {

    public String kind {get;set;} 
    public String id {get;set;} 
    public String etag {get;set;} 
    public String selfLink {get;set;} 
    public BookInfo volumeInfo{get;set;}
    //public ImageDetail imageLinks{get;set;}
    
    public class BookInfo{
        public String title {get;set;}
        public List <String> authors{get;set;}
        public List <ISBNDetail> industryIdentifiers{get;set;} 
        public String publisher{get;set;}
        public String publishedDate{get;set;}
        public String description{get;set;}
        public string averageRating{get;set;}
        public ImageDetail imageLinks{get;set;}

    }
    public class ImageDetail {
        public String smallThumbnail{get;set;}
        public String thumbnail{get;set;}
    
    }
    public class ISBNDetail {
        public String type{get;set;}
        public String identifier{get;set;}
    
    }

}